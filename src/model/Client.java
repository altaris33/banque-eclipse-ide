package model;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;

public class Client {
	
	// Attributs
	private int code;
	private String nom;
	private String email;
	private int codeCompte;
	
	public Client() {
		super();
	}

	public Client(int code, String nom, String email, int codeCompte) {
		super();
		this.code = code;
		this.nom = nom;
		this.email = email;
		this.codeCompte = codeCompte;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCodeCompte() {
		return codeCompte;
	}

	public void setCodeCompte(int codeCompte) {
		this.codeCompte = codeCompte;
	}

	// ToString
	@Override
	public String toString() {
		return "Client [code=" + code + ", nom=" + nom + ", email=" + email + ", codeCompte=" + codeCompte + "]";
	}

}
