package model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

public class Compte {
	
	// Attributs
	protected int codeCompte;
	protected LocalDate dateCreation;
	protected double solde;
	protected int codeClient;
	
	// Constructors
	public Compte() {
		super();
	}
	public Compte(int codeCompte, LocalDate dateCreation, double solde, int codeClient) {
		super();
		this.codeCompte = codeCompte;
		this.dateCreation = dateCreation;
		this.solde = solde;
		this.codeClient = codeClient;
	}
	
	// Getters & Setters
	public int getCodeCompte() {
		return codeCompte;
	}
	public void setCodeCompte(int codeCompte) {
		this.codeCompte = codeCompte;
	}
	public LocalDate getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}
	public double getSolde() {
		return solde;
	}
	public void setSolde(double solde) {
		this.solde = solde;
	}
	public int getCodeClient() {
		return codeClient;
	}
	public void setCodeClient(int codeClient) {
		this.codeClient = codeClient;
	}

	// ToString
	@Override
	public String toString() {
		return "- Compte N�         : " + codeCompte 
				+ "\n- Date reation : " + dateCreation 
				+ "\n- Solde        : " + solde + " �" 
				+ "\n- Client N�    : " + codeClient;
	}
}
